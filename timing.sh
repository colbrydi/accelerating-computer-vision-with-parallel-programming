#!/bin/bash
#SBATCH --gres=gpu:1
#SBATCH -N 4
#SBATCH -c 3
#SBATCH --mem 8gb
#SBATCH --time 00:20:00

make clear
make
time make test
