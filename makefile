all: traintest

FILE:=breastmnist

traintest: download.py
	python download.py --data_name $(FILE) --input_root input
	
test: traintest
	python train.py --data_name $(FILE) --input_root input --output_root output --num_epoch 10 --download True

clean:
	rm -r input
	rm -r output
